module Main where

import qualified Hw1 as H
import Draw

main :: IO ()
main = do H.sierpinskiCarpet
          H.myFractal
          H.mainXML
