

-- parser return a list of possible results
newtype Parser a = P (String -> [(a, String)])

doParse :: Parser t -> String -> [(t, String)]
doParse (P p) s = p s

oneChar :: Parser Char
oneChar = P (\cs -> case cs of
               c:cs' -> [(c, cs')]
               _     -> [])


